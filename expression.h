#ifndef EXPRESSION_H
#define EXPRESSION_H

#include <stdio.h>
#include <stdlib.h>

typedef enum {BLANK, PLUS, TIMES, MINUS, NUM, VAR, ERROR} exprType;

struct expression;
typedef struct expression Expression;

struct expression {
    exprType type;
    int value;
    int var;
    Expression *left;
    Expression *right;
};

void printEx(Expression *ex);

void initEx(Expression *ex);

void clearEx(Expression ex);

#endif /* EXPRESSION_H */
