#ifndef PARSE_H
#define PARSE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "expression.h"
#include "list.h"
#include "constants.h"

void initParser(int newV);

void setWhich(int newWhich);

Expression parseEx(char *exStr);

void parseInit(char *initStr, int *evaluate, int *valueSet);

void clearDep();

#endif /* PARSE_H */
