#include "constants.h"
#include "expression.h"
#include "list.h"
#include "parse.h"
#include "solver.h"

int main() {
    int N, K, V, M;
    int which;
    int error = 0;
    int tmp;

    Expression equation[MAX_VARIABLES];
    char toParse[MAX_LENGTH];

    scanf("%d %d %d\n", &N, &K, &V);

    M = N - K;

    for (int i = 0; i < V; i++) {
        initEx(&equation[i]);
    }

    initParser(V);

    for (int i = 0; i < K && error == 0; i++) {
        scanf("%d x[%d] = ", &tmp, &which);
        fgets(toParse, MAX_LENGTH, stdin);

        if (equation[which].type == BLANK) {

            setWhich(which);
            equation[which] = parseEx(toParse);

            if (equation[which].type != ERROR) {
                printf("%d P\n", i+1);
            } else {
                printf("%d F\n", i+1);
                error = 1;
            }
        } else {
            printf("%d F\n", i+1);
            error = 1;
        }
    }

    clearDep();

    if (!error)
        solve(K, M, V, equation);

    for (int i = 0; i < V; i++) {
        clearEx(equation[i]);
    }

    return 0;
}
