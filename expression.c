#include "expression.h"

void printEx(Expression *ex) {
    if (ex == NULL)
        return;
    printf("Expression: %d %d %d\n", ex->type, ex->value, ex->var);
    printf("Left:\n");
    printEx(ex->left);
    printf("Right:\n");
    printEx(ex->right);
    printf("End of Expression\n");
}

void initEx(Expression *ex) {
    ex->type = BLANK;
    ex->value = 0;
    ex->var = -1;
    ex->left = NULL;
    ex->right = NULL;
}

void clearEx(Expression ex) {
    if (ex.left != NULL) {
        clearEx(*(ex.left));
        free(ex.left);
    }
    if (ex.right != NULL) {
        clearEx(*(ex.right));
        free(ex.right);
    }
}
