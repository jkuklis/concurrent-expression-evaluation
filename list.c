#include "list.h"

Node makeNode(int value) {
    Node res;
    res.value = value;
    res.next = malloc(sizeof(*(res.next)));
    return res;
}

int find(int toFind, Node *toCheck) {
    if (toCheck->value == -1)
        return 0;
    else if (toCheck->value == toFind)
        return 1;
    else
        return find(toFind, toCheck->next);
}

void initNode(Node *toInit) {
    toInit->value = -1;
    toInit->next = NULL;
}

void printNode(Node *toCheck) {
    if (toCheck->value != -1) {
        printf("%d, ", toCheck->value);
        printNode(toCheck->next);
    }
}

void addValue(Node *toCheck, int value) {
    Node toPut = makeNode(value);
    *(toPut.next) = *toCheck;
    *toCheck = toPut;
}

void clearNode(Node *toClear) {
    if (toClear->value != -1) {
        clearNode(toClear->next);
        free(toClear->next);
    }
}
