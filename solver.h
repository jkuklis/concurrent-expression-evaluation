#ifndef SOLVER_H
#define SOLVER_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <poll.h>
#include "constants.h"
#include "expression.h"
#include "parse.h"
#include "list.h"

struct pair;
typedef struct pair Pair;

struct pair {
    int success;
    int value;
};

void solve(int K, int M, int v, Expression *equation);

#endif
