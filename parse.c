#include "parse.h"

int which;
int V;
int circular = 0;
int visited[MAX_VARIABLES];
int already[MAX_VARIABLES];
Node depends[MAX_VARIABLES];

void initParser(int newV) {
    V = newV;
    for (int i = 0; i < V; i++) {
        initNode(&depends[i]);
    }
}

void setWhich(int newWhich) {
    which = newWhich;
    for (int i = 0; i < V; i++) {
        already[i] = 0;
    }
}

void clearDep() {
    for (int i = 0; i < V; i++) {
        clearNode(&(depends[i]));
    }
}

void dfs(Node start) {
    if (circular)
        return;
    if (start.value != -1) {
        if (start.value == which) {
            circular = 1;
            return;
        }
        if (visited[start.value] == 0) {
            visited[start.value] = 1;
            dfs(depends[start.value]);
        }
        dfs(*(start.next));
    }
}

void checkCircular(int start) {
    if (already[start] == 0) {
        already[start] = 1;
        for (int i = 0; i < MAX_VARIABLES; i++) {
            visited[i] = 0;
        }
        visited[start] = 1;
        dfs(depends[start]);
    }
}

Expression parseEx(char *exStr) {
    int len = strlen(exStr);
    int count = 0;
    int position = 0;
    int condition = 1;
    char substr[len];

    Expression ex;
    initEx(&ex);

    if (exStr[len - 1] == '\n') {
        exStr[len - 1] = '\0';
        len--;
    }

    if (exStr[0] == '(') {
        ex.left = malloc(sizeof(*(ex.left)));
        initEx(ex.left);

        if (exStr[1] == '-') {
            ex.type = MINUS;

            strncpy(substr, exStr+2, len-3);
            substr[len-3] = '\0';

            Expression inverse = parseEx(substr);

            *(ex.left) = inverse;

            if (inverse.type == ERROR)
                ex.type = ERROR;

        } else {
            ex.right = malloc(sizeof(*(ex.right)));
            initEx(ex.right);

            while(condition) {
                switch(exStr[position]) {
                case '(':
                    count++;
                    break;
                case ')':
                    count--;
                    break;
                case '+':
                case '*':
                    if (count == 1) {
                        condition = 0;
                        ex.type = (exStr[position] == '+' ? PLUS : TIMES);

                        strncpy(substr, exStr+1, position-2);
                        substr[position-2] = '\0';

                        Expression left = parseEx(substr);

                        *(ex.left) = left;

                        strncpy(substr, exStr+position+2, len-position-3);
                        substr[len-position-3] = '\0';

                        Expression right = parseEx(substr);

                        *(ex.right) = parseEx(substr);

                        if (left.type == ERROR || right.type == ERROR)
                            ex.type = ERROR;

                    }
                    break;
                }
                position++;
            }
        }
    } else if (exStr[0] == 'x') {
        ex.type = VAR;

        strncpy(substr, exStr+2, len-3);
        substr[len-3] = '\0';

        ex.var = atoi(substr);

        checkCircular(ex.var);

        if (which == ex.var || circular != 0)
            ex.type = ERROR;

        if (find(ex.var, &(depends[which])) == 0) {
            addValue(&(depends[which]), ex.var);
        }

    } else {
        ex.type = NUM;
        ex.value = atoi(exStr);
    }
    return ex;
}


void parseInit(char *initStr, int *evaluate, int *valueSet) {
    if (initStr[0] == '\0' || initStr[0] == '\n')
        return;

    char substr[strlen(initStr)];

    int position = 3;
    while (initStr[position] != ']')
        position++;

    strncpy(substr, initStr+2, position-2);

    int a = atoi(substr);

    position += 2;

    int end = position+1;

    while (isdigit(initStr[end]))
        end++;

    strncpy(substr, initStr+position, end-position);

    substr[end-position] = '\0';

    int b = atoi(substr);

    evaluate[a] = b;
    valueSet[a] = 1;

    if (initStr[end] != '\0' && initStr[end] != '\n')
        parseInit(initStr+end+1, evaluate, valueSet);
}
