#ifndef LIST_H
#define LIST_H

#include <stdio.h>
#include <stdlib.h>

struct node;
typedef struct node Node;

struct node {
    int value;
    Node *next;
};

Node makeNode(int value);

int find(int toFind, Node *toCheck);

void initNode(Node *toInit);

void printNode(Node *toPrint);

void addValue(Node *toAdd, int value);

void clearNode(Node *toClear);

#endif /* LIST_H */
