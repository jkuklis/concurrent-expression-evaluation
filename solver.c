#include "solver.h"

int disabled = 0;
int evaluate[MAX_VARIABLES];
int valueSet[MAX_VARIABLES];

char initList[MAX_LENGTH];

Pair makePair(int success, int value) {
    Pair res;
    res.success = success;
    res.value = value;
    return res;
}

Pair calcValue(Expression ex, Expression *equation) {
    Pair left = makePair(1, 0);
    Pair right = makePair(1, 0);
    Pair res = makePair(1, 0);

    switch (ex.type) {
    case BLANK:
        res.success = 0;
        break;

    case NUM:
        res.value = ex.value;
        break;

    case MINUS:
        res = calcValue(*(ex.left), equation);
        res.value *= -1;
        break;

    case PLUS:
    case TIMES:
        right = calcValue(*(ex.right), equation);

        left = calcValue(*(ex.left), equation);

        if (ex.type == PLUS)
            res.value = left.value + right.value;
        else
            res.value = left.value * right.value;

        if (right.success == 0 || left.success == 0)
            res.success = 0;
        else
            res.success = 1;

        if (ex.type == TIMES)
            if ((left.success && left.value == 0) || (right.success && right.value == 0))
                res.success = 1;

        break;

    case VAR:

        if (valueSet[ex.var] == 1) {
            res.success = 1;
            res.value = evaluate[ex.var];

        } else if (valueSet[ex.var] == 0) {
            res = calcValue(equation[ex.var], equation);

            if (res.success == 1) {
                valueSet[ex.var] = 1;
                evaluate[ex.var] = res.value;
            } else {
                valueSet[ex.var] = -1;
            }

        } else {
            res.success = 0;
        }

        break;

    default:
        break;
    }

    return res;
}


void process(int nr, int V, Expression *equation) {
    int position = 0;

    while (initList[position] != ' ' && initList[position] != '\0')
        position++;

    if (equation[0].type == BLANK) {
        printf("%d F\n", nr);
        return;
    }

    if (initList[position] != '\0')
        parseInit(initList+position+1, evaluate, valueSet);

    if (valueSet[0] == 1) {
        printf("%d P %d\n", nr, evaluate[0]);
    } else {
        Pair res = calcValue(equation[0], equation);
        if (res.success == 1) {
            printf("%d P %d\n", nr, res.value);
        } else {
            printf("%d F\n", nr);
        }
    }
}

void solve(int K, int M, int V, Expression *equation) {

    for (int i = 0; i < V; i++) {
        evaluate[i] = 0;
        valueSet[i] = 0;
    }

    for (int i = 0; i < M && disabled == 0; i++) {
        initList[0] = '\0';
        fgets(initList, MAX_LENGTH, stdin);

        switch (fork()) {
        case -1:
            break;
        case 0:
            process(K+i+1, V, equation);
            disabled = 1;
            break;
        default:
            break;
        }
    }

    wait(0);
}
